/**
 * Retourne des élements aléatoires depuis un tableau
 * @param {int[]} elements
 * @param {int} nbElement
 */
export const getRandomElementsFromArray = (elements, nbElement, mustNotIn) => {
  const randomElements = []
  for (let i = 0; i < nbElement; i++) {
    const random = Math.floor(Math.random() * elements.length)

    if (
      (mustNotIn &&
        mustNotIn.length &&
        !mustNotIn.includes(elements[random])) ||
      !mustNotIn
    ) {
      randomElements.push(elements[random])
    }
    elements.splice(random, 1)
  }

  if (!randomElements.length) {
    return getRandomElementsFromArray(elements, nbElement, mustNotIn)
  }
  return randomElements
}
